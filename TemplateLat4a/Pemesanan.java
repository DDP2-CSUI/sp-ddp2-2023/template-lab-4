package TemplateLat4a;

class Pemesanan {

    public static void main(String[] args) {
        
        System.out.println("Selamat datang! Berikut ini merupakan daftar unit apartemen yang ada di Pak Depe Fancy Flats:");

        // TODO: Inisiasi objek unit-unit apartemen yang Pak Depe miliki sesuai keterangan soal
        Apartemen A1001 = null;
        Apartemen D0211 = null;
        Apartemen B0823 = null;

        Apartemen[] daftarApartemen = new Apartemen[3];
        daftarApartemen[0] = A1001;
        daftarApartemen[1] = D0211;
        daftarApartemen[2] = B0823;


        // TODO: Print daftar unit Pak Depe beserta informasinya sesuai keterangan soal
        for (int i = 0; i<daftarApartemen.length; i++) {
            


            
        }

        System.out.println();
        System.out.println("/////////////////////////////////////////// RUN PROGRAM /////////////////////////////////////////////");
        System.out.println("=====================================================================================================");

        // Beberapa orang ingin melakukan check in pada kamar-kamar yang tersedia
        System.out.println(A1001.checkIn());
        System.out.println(D0211.checkIn());
        System.out.println(B0823.checkIn());
        System.out.println(B0823.checkIn());
        
        // Penyewa kamar nomor A1001 mengajukan komplain!
        System.out.println(A1001.ajukanKomplain());
        System.out.println(A1001.ajukanKomplain());
        System.out.println(A1001.ajukanKomplain());
        System.out.println(A1001.ajukanKomplain());
        
        // Penyewa kamar nomor A1001 melakukan check out di hari ke-30
        System.out.println(A1001.checkOut(30, 5));
        // Ada yang check out lagi ???
        System.out.println(A1001.checkOut(5, 1));
        
        // Ada beberapa orang yang mengajukan komplain!
        System.out.println(A1001.ajukanKomplain());
        System.out.println(B0823.ajukanKomplain());

        // Penyewa kamar nomor B0823 melakukan check out di hari ke-5
        System.out.println(B0823.checkOut(5, 3));
        
        // Ada yang kembali menyewa kamar nomor B0823
        System.out.println(B0823.checkIn());

        // Komplain datang dari kamar B0823!
        System.out.println(B0823.ajukanKomplain());
        System.out.println(B0823.ajukanKomplain());
        System.out.println(B0823.ajukanKomplain());

        // Penyewa kamar nomor B0823 melakukan check out di hari ke-5
        System.out.println(B0823.checkOut(5, 3));

        System.out.println("Terimakasih, sampai jumpa di lain kesempatan!");
    }
}