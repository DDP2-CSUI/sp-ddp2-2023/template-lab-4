package TemplateLat4a;

public class Apartemen {
    private String nomorKamar;
    private boolean terisi;
    private String[] fasilitas = new String[3]; 
    private int hargaPerHari;
    private int jumlahKomplain;
    private double rating;

    // TODO: lengkapi constructor objek Apartemen
    public Apartemen(String nomorKamar, boolean terisi, int harga, String[] fasilitas, double rating) {
        
        this.jumlahKomplain = 0;
    }

    // Increment jumlah komplain setiap ada yang mengajukan komplain
    public String ajukanKomplain() {
        if (this.terisi == true) {
            this.jumlahKomplain++;
            return ("Terima kasih telah mengajukan komplain untuk kamar " + this.nomorKamar);
        } else {
            return ("Komplain hanya bisa diajukan jika kamar sedang dalam masa diisi");
        }
    }



    // TODO: Implementasi method check in di bawah ini
    public String checkIn() {
        return "";
    }



    // TODO: Implementasi method check out di bawah ini
    public String checkOut(int hari, int rating) {
        return "";
    }



    // TODO: buatkan method getter & setter untuk tiap atribut

    // Perhitungan harga dengan diskon 50% jika komplain > 3 kali
    public int getTotalHarga(int hari) {
        if (this.jumlahKomplain > 3) {
            return this.hargaPerHari * hari * 1/2;
        } else {
            return this.hargaPerHari * hari;
        }
    }

    // Set rating baru jika ada yang checkout
    public void setRating(int ratingBaru) {
        this.rating = (this.rating + ratingBaru) / 2;
    }
}
    