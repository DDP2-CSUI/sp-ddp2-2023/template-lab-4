public class Siperpus {
    public static void main(String[] args) {
        Buku thinkJava = new Buku("Think Java", "Allen B. Downey", "Green Tea Press", 10);
        Buku basicPython = new Buku("Python for Beginners","Timothy C. Needham", "Amazon", 2);
        Buku calculus = new Buku("Calculus", "Purcell", "Illinois", 1);
        Buku adProg = new Buku("Head First Design Patterns", "Freeman", "O\'Reilly", 11);
        Buku discreteMath = new Buku("Discrete Mathematics and Its Applications", "Rosen", "Mc Grawhill", 8);
        
        Alamat alamat1 = new Alamat("Jalan Sesama", 10, "Graha", "Pondok", "Jaktim");
        Alamat alamat2 = new Alamat("Jalan Bahagia", 99, "Indah", "Kramat", "Jakbar");
        Alamat alamat3 = new Alamat("Jalan Kenangan", 5, "Pinang", "Ranti", "Jakpus");

        Anggota yan = new Anggota("Yan", "Ilmu Komputer", "2021", alamat1);
        Anggota fajar = new Anggota("Fajar", "Sistem Informasi", "2021", alamat2);
        Anggota fikri = new Anggota("Fikri", "Ilmu Komputer", "2019", alamat3);

        yan.pinjam(thinkJava);
        yan.pinjam(basicPython);
        yan.kembali(discreteMath);
        yan.kembali(thinkJava);

        fajar.pinjam(calculus);
        fajar.pinjam(discreteMath);
        fajar.pinjam(basicPython);
        fajar.kembali(discreteMath);

        fikri.pinjam(calculus);
        fikri.pinjam(adProg);
        fikri.pinjam(discreteMath);
        fikri.pinjam(basicPython);
        fikri.kembali(basicPython);
        fikri.kembali(calculus);

        System.out.println("-----------------");
        System.out.println(fajar +"\n");
        System.out.println(yan + "\n");
        System.out.println(fikri + "\n");
    }
}
